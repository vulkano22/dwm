/* See LICENSE file for copyright and license details. */
#define PrintScreenDWM	    0x0000ff61
/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 3;       /* gap pixel between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {"mononoki Nerd Font:size=12"};
static const char dmenufont[]       = "mononoki Nerd Font:size=12";
static const char col_gray1[]       = "#3b4252";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_gray5[]       = "#ffffff";
static const char col_cyan[]        = "#005577";
static const char col_red1[]        = "#ff0000";
static const char col_red2[]        = "#ff5555";
static const char col_green[]       = "#809575";
static const char col_green1[]      = "#809575";
static const char col_blue[]        = "#5C6BC0";
static const char col_black[]       = "#000000";
static const char *colors[][3]      = {
	/*                    fg         bg         border   */
//	[SchemeNorm]     = { col_gray3, col_gray1, col_gray2 },
//	[SchemeSel]      = { col_gray4, col_red1,  col_green1  },
//	[SchemeStatus]   = { col_gray5, col_gray1, col_green1  }, // Statusbar right {text,background,not used but cannot be empty}
//	[SchemeTagsSel]  = { col_gray5, col_green, col_green1  }, // Tagbar left selected {text,background,not used but cannot be empty}
//  [SchemeTagsNorm] = { col_gray5, col_gray1, col_green1  }, // Tagbar left unselected {text,background,not used but cannot be empty}
//  [SchemeInfoSel]  = { col_gray1, col_green, col_green1  }, // infobar middle  selected {text,background,not used but cannot be empty}
//	[SchemeInfoNorm] = { col_gray1, col_green, col_green1  }, // infobar middle  unselected {text,background,not used but cannot be empty}
// };

	/*                    fg         bg         border   */
	[SchemeNorm]     = { col_gray3, col_black, col_black },
	[SchemeSel]      = { col_gray4, col_black, col_black  },
	[SchemeStatus]   = { col_gray5, col_black, col_black  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_gray5, col_gray3, col_black  }, // Tagbar left selected {text,background,not used but cannot be empty}
  [SchemeTagsNorm] = { col_gray5, col_black, col_black  }, // Tagbar left unselected {text,background,not used but cannot be empty}
  [SchemeInfoSel]  = { col_gray1, col_black, col_black  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm] = { col_gray1, col_black, col_black  }, // infobar middle  unselected {text,background,not used but cannot be empty}
 };

/* tagging */
static const char *tags[] = {"", "", "﬏", "", "", "", "", "", ""};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Alacritty",   	NULL,       NULL,       1 << 0,       0,           -1 },
	{ "Gimp",     		NULL,       NULL,       1 << 2,       0,           -1 },
	{ "firefox",  		NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Chromium", 		NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Code",     		NULL,       NULL,       1 << 2,       0,           -1 },
  	{ "Teams",    		NULL,       NULL,       1 << 3,       0,           -1 },
  	{ "Thunderbird",	NULL,       NULL,       1 << 4,       0,           -1 },
  	{ "stm32cubeide",	NULL,       NULL,       1 << 5,       0,           -1 },
  	{ "keepass2",	    NULL,       NULL,       1 << 6,       0,           -1 },
  	{ "nautilus",	    NULL,       NULL,       1 << 7,       0,           -1 },
  	{ "vlc",	        NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};


#include <X11/XF86keysym.h>

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]      = { "dmenu_run", "-c", "-l", "20", "-bw", "2",                   NULL };
static const char *termcmd[]       = { "alacritty",                                            NULL };
static const char *upvol[]         = { "pulsemixer", "--change-volume", "+5",                  NULL };
static const char *downvol[]       = { "pulsemixer", "--change-volume", "-5",                  NULL };
static const char *mutevol[]       = { "pulsemixer", "--toggle-mute",                          NULL };
static const char *togglemic[]     = { "amixer", "sset", "Capture,0", "toggle",                NULL };
static const char *firefox[]       = { "firefox",                                              NULL };
static const char *brave[]         = { "brave-browser",                                        NULL };
static const char *chromium[]      = { "chromium",   "--incognito",                            NULL };
static const char *slock[]         = { "slock",                                                NULL };
static const char *nautilus[]      = { "nautilus",                                             NULL };
static const char *flameshot[]     = { "flameshot", "gui",                                     NULL };
static const char *rofi_launcher[] = { "/home/$USER/software/rofi/1080p/launchers/misc/launcher.sh",    NULL };
//static const char *qalculator[] = { "qalculate-gtk",  NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1  } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1  } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1  } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1  } },
  	/*===========================CUSTOM SHORTCUTS==========================================*/
	{ MODKEY|ShiftMask,             XK_f,      spawn,          {.v = firefox  } },
	{ MODKEY|ShiftMask,             XK_b,      spawn,          {.v = brave  } },
  	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = slock    } },
  	{ MODKEY|ShiftMask,             XK_l,      spawn,          {.v = rofi_launcher } },
  	{ MODKEY|ShiftMask,             XK_g,      spawn,          {.v = chromium } },
  	{ MODKEY|ShiftMask,             XK_n,      spawn,          {.v = nautilus } },
  	{ MODKEY,                       XK_F1,     spawn,          {.v = mutevol } },
  	{ MODKEY,                       XK_F2,     spawn,          {.v = downvol } },
  	{ MODKEY,                       XK_F3,     spawn,          {.v = upvol } },
  	{ MODKEY|ShiftMask,             XK_m,      spawn,          {.v = togglemic } },
  	{0,                  XF86XK_AudioMute,     spawn,          {.v = mutevol } },
  	{0,           XF86XK_AudioLowerVolume,     spawn,          {.v = downvol } },
  	{0,           XF86XK_AudioRaiseVolume,     spawn,          {.v = upvol } },
  	{0,                    PrintScreenDWM,     spawn,          {.v = flameshot } },
	/*=====================================================================================*/
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};


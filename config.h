/* See LICENSE file for copyright and license details. */
#define PrintScreenDWM	    0x0000ff61
/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 6;       /* gap pixel between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;     /* 0 means no bar */
static const int topbar             = 1;     /* 0 means bottom bar */
static const char *fonts[]          = {"mononoki Nerd Font:size=11"};
static const char dmenufont[]       = "mononoki Nerd Font:size=12";
static const char col_background[]  = "#2E3440";
static const char col_arch_blue[]   = "#1793d1";
static const char col_white[]       = "#ffffff";
static const char col_gray1[]       = "#3b4252";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_gray5[]       = "#ffffff";
static const char col_cyan[]        = "#005577";
static const char col_red1[]        = "#ff0000";
static const char col_red2[]        = "#ff5555";
static const char col_green[]       = "#809575";
static const char col_green1[]      = "#809575";
static const char col_blue[]        = "#5C6BC0";
static const char col_black[]       = "#000000";
static const char *colors[][3]      = {

	/*                    fg         bg         border   */
	[SchemeNorm]     = { col_white, col_background, col_black },
	[SchemeSel]      = { col_white, col_arch_blue,  col_arch_blue },
	[SchemeStatus]   = { col_white, col_background, col_black  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_white, "#222222", col_black  }, // Tagbar left selected {text,background,not used but cannot be empty}
    [SchemeTagsNorm] = { col_white, col_background, col_black  }, // Tagbar left unselected {text,background,not used but cannot be empty}
    [SchemeInfoSel]  = { col_white, col_background, col_black  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm] = { col_white, col_background, col_black  }, // infobar middle  unselected {text,background,not used but cannot be empty}
 };

/* tagging */
static const char *tags[] = {"", "", "﬏", "", "", "", "", "", ""};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
//{ "alacritty",   	NULL,       NULL,       1 << 0,       0,           -1 },
	{ "Gimp", NULL, NULL, 1 << 2, 0, -1 },
	{ "firefox", NULL, NULL, 1 << 1, 0, -1 },
	{ "chromium", NULL, NULL, 1 << 1, 0, -1 },
	{ "code", NULL, NULL, 1 << 2, 0, -1 },
  	{ "teams", NULL, NULL, 1 << 4, 0, -1 },
  	{ "microsoft teams - preview", NULL, NULL, 1 << 4, 0, -1 },
  	{ "Microsoft Teams - Preview", NULL, NULL, 1 << 4, 0, -1 },
  	{ "thunderbird", NULL, NULL, 1 << 5, 0, -1 },
  	{ "mailspring", NULL, NULL, 1 << 5, 0, -1 },
  	{ "Mailspring", NULL, NULL, 1 << 5, 0, -1 },
  	{ "STM32CubeIDE", NULL, NULL, 1 << 3, 0, -1 },
  	{ "KeePassXC", NULL, NULL, 1 << 6, 0, -1 },
  	{ "vlc", NULL, NULL, 1 << 8, 0, -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};


#include <X11/XF86keysym.h>

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]           = { "dmenu_run", "-c", "-l", "20", "-bw", "2",              NULL };
static const char *dmenu_kill[]         = { "dmenu-kill",                                           NULL };
// static const char *keepassxc_dmenu[]    = { "dmenu-keepassxc",                                      NULL };
static const char *termcmd[]            = { "st",                                                   NULL };
static const char *upvol[]              = { "pulsemixer", "--change-volume", "+5",                  NULL };
static const char *downvol[]            = { "pulsemixer", "--change-volume", "-5",                  NULL };
static const char *mutevol[]            = { "pulsemixer", "--toggle-mute",                          NULL };
static const char *togglemic[]          = { "pulsemixer", "--toggle-mute", "--id", "58",            NULL };
static const char *firefox[]            = { "firefox",                                              NULL };
static const char *slock[]              = { "slock",                                                NULL };
static const char *flameshot[]          = { "flameshot", "gui",                                     NULL };
static const char *rofi_launcher[]      = { "rofi-launcher",           NULL };
static const char *rofi_powermenu[]      = { "rofi-powermenu",           NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } } ,
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = rofi_powermenu } } ,
	{ MODKEY|ShiftMask,             XK_k,      spawn,          {.v = dmenu_kill } } ,
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1  } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1  } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1  } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1  } },
  	/*===========================CUSTOM SHORTCUTS==========================================*/
	{ MODKEY|ShiftMask,             XK_f,      spawn,          {.v = firefox  } },
  	{ MODKEY,             XK_s,      spawn,          {.v = slock    } },
  	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = flameshot    } },
  	{ MODKEY,                       XK_r,      spawn,          {.v = rofi_launcher } },
  	{ MODKEY,                       XK_F1,     spawn,          {.v = mutevol } },
  	{ MODKEY,                       XK_F2,     spawn,          {.v = downvol } },
  	{ MODKEY,                       XK_F3,     spawn,          {.v = upvol } },
  	{ MODKEY|ShiftMask,             XK_m,      spawn,          {.v = togglemic } },
  	{0,                  XF86XK_AudioMute,     spawn,          {.v = mutevol } },
  	{0,           XF86XK_AudioLowerVolume,     spawn,          {.v = downvol } },
  	{0,           XF86XK_AudioRaiseVolume,     spawn,          {.v = upvol } },
  	{0,                    PrintScreenDWM,     spawn,          {.v = flameshot } },
	/*=====================================================================================*/
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_NONE}   )
};

